\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduzione}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Il Progetto}{2}{section.1.1}
\contentsline {paragraph}{}{2}{section*.2}
\contentsline {paragraph}{}{2}{section*.3}
\contentsline {section}{\numberline {1.2}Le specifiche}{2}{section.1.2}
\contentsline {paragraph}{}{2}{section*.4}
\contentsline {section}{\numberline {1.3}Una panoramica}{2}{section.1.3}
\contentsline {paragraph}{}{2}{section*.5}
\contentsline {subsection}{\numberline {1.3.1}Architettura}{2}{subsection.1.3.1}
\contentsline {paragraph}{}{2}{section*.6}
\contentsline {paragraph}{}{3}{section*.7}
\contentsline {subsection}{\numberline {1.3.2}Gli scenari}{3}{subsection.1.3.2}
\contentsline {paragraph}{}{3}{section*.8}
\contentsline {paragraph}{}{3}{section*.9}
\contentsline {section}{\numberline {1.4}Perch\IeC {\'e} gli \emph {EEG}?}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Esecuzione}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Utilizzo}{4}{section.2.1}
\contentsline {paragraph}{}{4}{section*.10}
\contentsline {paragraph}{}{4}{section*.11}
\contentsline {section}{\numberline {2.2}Alcoholic}{4}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Dataset}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Esecuzione}{5}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}output}{6}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Wrist}{6}{section.2.3}
\contentsline {paragraph}{}{6}{section*.12}
\contentsline {section}{\numberline {2.4}Modalit\IeC {\`a} unattended}{6}{section.2.4}
\contentsline {paragraph}{}{7}{section*.13}
\contentsline {chapter}{\numberline {3}Dataset: Alcoholic}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Interpretazione del dataset}{8}{section.3.1}
\contentsline {paragraph}{}{8}{section*.14}
\contentsline {paragraph}{}{8}{section*.15}
\contentsline {section}{\numberline {3.2}Classificazione: un approccio bottom up}{9}{section.3.2}
\contentsline {paragraph}{}{9}{section*.16}
\contentsline {paragraph}{}{9}{section*.17}
\contentsline {paragraph}{}{9}{section*.18}
\contentsline {section}{\numberline {3.3}Il test}{9}{section.3.3}
\contentsline {paragraph}{}{10}{section*.19}
\contentsline {paragraph}{}{10}{section*.20}
\contentsline {paragraph}{}{12}{section*.21}
\contentsline {section}{\numberline {3.4}University of California, Irvine - Knowledge Discovery Database Archive}{12}{section.3.4}
\contentsline {paragraph}{}{12}{section*.22}
\contentsline {chapter}{\numberline {4}Dataset: Wrist}{13}{chapter.4}
\contentsline {section}{\numberline {4.1}Interpretazione del dataset}{13}{section.4.1}
\contentsline {paragraph}{}{13}{section*.23}
\contentsline {section}{\numberline {4.2}Risultati}{13}{section.4.2}
\contentsline {paragraph}{}{14}{section*.24}
\contentsline {chapter}{\numberline {5}Conclusioni}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}La complessit\IeC {\`a} delle time series}{17}{section.5.1}
\contentsline {paragraph}{}{17}{section*.25}
\contentsline {paragraph}{}{17}{section*.26}
\contentsline {paragraph}{}{17}{section*.27}
\contentsline {chapter}{Bibliography}{18}{section*.27}
