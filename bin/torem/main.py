#! /usr/bin/python
from sklearn import tree
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import csv
import glob

def main():
  datapath='wrist_data/'
  data_files='%s*eeg.csv'%datapath
  target_labels_file='%star_class_named_labels.csv'%datapath
  test_size=0.2
  eeg_files=glob.glob(data_files)
  data = []
  predictions=[]
  predictions1=[]
  predictions2=[]
  newdict1={'left':'straight', 'straight':'right', 'right':'left'}
  newdict2={'left':'right', 'straight':'left', 'right':'straight'}
  for eeg_file in eeg_files:
    eeg=[]
    with open(eeg_file) as csvfile:
      reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC) # change contents to floats
      for row in reader: # each row is a list
        eeg += row
    data.append(eeg)
  with open(target_labels_file) as tlf:
      target=tlf.read().splitlines()
  data_train, data_test, target_train, target_test=\
      train_test_split(data, target, test_size=test_size)
  print len(data_train)
  tree_clf = tree.DecisionTreeClassifier()
  tree_clf.fit(data_train, target_train)
  # predict
  predictions.append( tree_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'Decision Tree\t\t\t\t%s'%( accuracy_score(target_test, predictions[-1]))
  nk_clf = KNeighborsClassifier()
  nk_clf.fit(data_train, target_train)
  # predict
  predictions.append( nk_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'K Neighbors\t\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  lsvc_clf = svm.LinearSVC()
  lsvc_clf.fit(data_train, target_train)
  # predict
  predictions.append( lsvc_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm linear svc\t\t\t\t%s'% (accuracy_score(target_test, predictions[-1]))
  svc_clf = svm.SVC(C=5., gamma=0.001)
  svc_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc - 5 0.001\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_clf = svm.SVC(C=2., gamma=0.001)
  svc_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc - 2 0.001\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_clf = svm.SVC(C=5., gamma=0.01)
  svc_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc - 5 0.01\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_lk_clf = svm.SVC(kernel='linear')
  svc_lk_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_lk_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc linear kernel\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk3_clf = svm.SVC(kernel='poly', degree=3, gamma='auto')
  svc_pk3_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk3_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=3 auto\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk3_clf = svm.SVC(kernel='poly', degree=3, gamma='scale')
  svc_pk3_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk3_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=3 scale\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk7_clf = svm.SVC(kernel='poly', degree=7, gamma='auto')
  svc_pk7_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk7_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=7 auto\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk7_clf = svm.SVC(kernel='poly', degree=7, gamma='scale')
  svc_pk7_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk7_clf.predict(data_test))
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=7 scale\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_rbfk_clf = svm.SVC(kernel='rbf', gamma='auto')
  svc_rbfk_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_rbfk_clf.predict(data_test))
  tmp1=[]
  tmp2=[]
  for item in predictions[-1]:
    tmp1.append(newdict1[item])
    tmp2.append(newdict2[item])
  predictions1.append(tmp1)
  predictions2.append(tmp2)
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc radial basis kernel - auto\t%s'%( accuracy_score(target_test, predictions[-1]))
  if float(accuracy_score(target_test, predictions1[-1])) > 0.5:
    print 'svm svc radial basis kernel - auto 1\t%s'%( accuracy_score(target_test, predictions1[-1]))
  if float(accuracy_score(target_test, predictions2[-1])) > 0.5:
    print 'svm svc radial basis kernel - auto 2\t%s'%( accuracy_score(target_test, predictions2[-1]))
  svc_rbfk_clf = svm.SVC(kernel='rbf', gamma='scale')
  svc_rbfk_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_rbfk_clf.predict(data_test))
  tmp1=[]
  tmp2=[]
  for item in predictions[-1]:
    tmp1.append(newdict1[item])
    tmp2.append(newdict2[item])
  predictions1.append(tmp1)
  predictions2.append(tmp2)
  if float(accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc radial basis kernel - scale\t%s'%(accuracy_score(target_test, predictions[-1]))
  if float(accuracy_score(target_test, predictions1[-1])) > 0.5:
    print 'svm svc radial basis kernel - scale 1\t%s'%(accuracy_score(target_test, predictions1[-1]))
  if float(accuracy_score(target_test, predictions2[-1])) > 0.5:
    print 'svm svc radial basis kernel - scale 2\t%s'%(accuracy_score(target_test, predictions2[-1]))
  
if __name__== "__main__":
  main()
