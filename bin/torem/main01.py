#! /usr/bin/python
#http://kdd.ics.uci.edu/databases/eeg/eeg.html


from sklearn import tree
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import csv
import glob
import random
import getopt
import sys

def main():
  try:
    opts, args = getopt.getopt(
        sys.argv[1:], 
        'hd:rv', 
        ['help', '--datapath', '--ratio'])
  except getopt.GetoptError as err:
    # print help information and exit:
    print str(err)  # will print something like "option -a not recognized"
    usage()
    sys.exit(2)
  output = None
  verbose = False
  ratio=0.2
  datapath='data20'
  for o, a in opts:
    if o == '-v':
      verbose = True
    elif o in ('-h', '--help'):
      usage()
      sys.exit()
    elif o in ('-d', '--datapath'):
      datapath = a
    elif o in ('-r', '--ratio'):
      ratio = a
    else:
      assert False, 'unhandled option'
  learn(datapath, ratio, verbose)

def usage():
  print 'python bin/main.py -d <data_path> -r <testing data ratio>'
 

def learn(datapath, ratio, verbose):
  eeg_files=glob.glob(datapath)
  random.shuffle(eeg_files)
  data = []
  target = []
  predictions=[]
  for eeg_file in eeg_files:
    with open(eeg_file) as eeg:
      #data.append(eeg.read().splitlines())
      data.append(map(float, eeg))
    if eeg_file[len(datapath)+2:len(datapath)+3] == 'a':
      target.append('alcoholic')
    else:
      target.append('control')
  data_train, data_test, target_train, target_test=\
      train_test_split(data, target, test_size=ratio)
  print len(data_train)
  tree_clf = tree.DecisionTreeClassifier()
  tree_clf.fit(data_train, target_train)
  # predict
  predictions.append( tree_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'Decision Tree\t\t\t\t%s'%( accuracy_score(target_test, predictions[-1]))
  nk_clf = KNeighborsClassifier()
  nk_clf.fit(data_train, target_train)
  # predict
  predictions.append( nk_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'K Neighbors\t\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  lsvc_clf = svm.LinearSVC()
  lsvc_clf.fit(data_train, target_train)
  # predict
  predictions.append( lsvc_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm linear svc\t\t\t\t%s'% (accuracy_score(target_test, predictions[-1]))
  svc_clf = svm.SVC(C=5., gamma=0.001)
  svc_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc - 5 0.001\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_clf = svm.SVC(C=2., gamma=0.001)
  svc_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc - 2 0.001\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_clf = svm.SVC(C=5., gamma=0.01)
  svc_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc - 5 0.01\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_lk_clf = svm.SVC(kernel='linear')
  svc_lk_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_lk_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc linear kernel\t\t\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk3_clf = svm.SVC(kernel='poly', degree=3, gamma='auto')
  svc_pk3_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk3_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=3 auto\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk3_clf = svm.SVC(kernel='poly', degree=3, gamma='scale')
  svc_pk3_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk3_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=3 scale\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk7_clf = svm.SVC(kernel='poly', degree=7, gamma='auto')
  svc_pk7_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk7_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=7 auto\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_pk7_clf = svm.SVC(kernel='poly', degree=7, gamma='scale')
  svc_pk7_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_pk7_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc polynomial kernel - d=7 scale\t%s'%(accuracy_score(target_test, predictions[-1]))
  svc_rbfk_clf = svm.SVC(kernel='rbf', gamma='auto')
  svc_rbfk_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_rbfk_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
    print 'svm svc radial basis kernel - auto\t%s'%( accuracy_score(target_test, predictions[-1]))
  svc_rbfk_clf = svm.SVC(kernel='rbf', gamma='scale')
  svc_rbfk_clf.fit(data_train, target_train)
  # predict
  predictions.append( svc_rbfk_clf.predict(data_test))
  if verbose or (accuracy_score(target_test, predictions[-1])) > 0.5:
   print 'svm svc radial basis kernel - scale\t%s'%(accuracy_score(target_test, predictions[-1]))
  
if __name__== "__main__":
  main()
