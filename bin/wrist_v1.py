#! /usr/bin/python


from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, log_loss
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.metrics import precision_recall_curve
from sklearn.utils.fixes import signature
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv
import glob
import random
import getopt
import sys
import os


datapath='wrist_data/*eeg.csv'
target_labels_file='wrist_data/tar_class_labels.csv'
classifiers = [
    (KNeighborsClassifier(3), 'K Nearest Neighbors' ),
    (SVC(kernel="linear", C=0.025, gamma='scale'), 'Linear SVM'),
    (SVC(gamma=2, C=1), 'RBF SVM'),
    (LinearSVC(), 'Linear SVC'),
    (GaussianProcessClassifier(1.0 * RBF(1.0)), 'Gaussian Process'),
    (DecisionTreeClassifier(max_depth=5), 'Decision Tree depth 5'),
    (DecisionTreeClassifier(max_depth=10), 'Decision Tree depth 10'),
    (RandomForestClassifier(
        max_depth=5, 
        n_estimators=10, 
        max_features=1), 'Random Forest'),
    (GradientBoostingClassifier(), 'Gradient Boosting Classifier'),
    (MLPClassifier(alpha=1), 'Neural Net'),
    (AdaBoostClassifier(), 'Ada Boost'),
    (GaussianNB(), 'Naive Baies'),
    (LinearDiscriminantAnalysis(), 'Linear  Discriminant Analysis'),
    (QuadraticDiscriminantAnalysis(),'QDA')
    ]

final_report_cols=[
    'Dataset',
    'Dataset Size',
    'Test Size',
    'Classifier',
    'Accuracy',
    'AVG Precision',
    'AVG Recall',
    'F1 score']
final_report= pd.DataFrame(columns=final_report_cols)


def main():
  try:
    opts, args = getopt.getopt(
        sys.argv[1:], 
        'hr:t:o:v', 
        ['help', '--ratio', '--target_mode'])
  except getopt.GetoptError as err:
    print str(err) 
    usage()
    sys.exit(2)
  output = None
  verbose = False
  ratio=0.2
  datapath=None
  output_path='out'
  for o, a in opts:
    if o == '-v':
      verbose = True
    elif o in ('-h', '--help'):
      usage()
      sys.exit()
    elif o in ('-r', '--ratio'):
      ratio = float(a)
    elif o in ('-o', '--output'):
      output_path = a
    else:
      assert False, 'unhandled option'
  learn(ratio, verbose)
  write_final_report(output_path, datapath)

def usage():
  usage_message='''usage:
  
  $ python bin/main.py \
  -r <testing data ratio> \
  -o <output path>
  
  example: 
  $ ./bin/oinos.py  -r 0.3 -v -o out
  '''
  print usage_message
 

def learn(ratio, verbose):
  eeg_files=glob.glob(datapath)
  random.shuffle(eeg_files)
  data = []
  target = []
  for eeg_file in eeg_files:
    eeg=[]
    with open(eeg_file) as csvfile:
      reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
      for row in reader:
        eeg += row
    data.append(eeg)
  with open(target_labels_file) as tlf:
      target=tlf.read().splitlines()
  print '=========      LEFT - CENTRAL - RIGHT      ============'
  learn_target(
      data, 
      target, 
      ratio, 
      'WRIST', 
      verbose)


def learn_target(
    data,
    target, 
    ratio, 
    tag, 
    verbose):  
  data_train, data_test, target_train, target_test=\
      train_test_split(data, target, test_size=ratio)
  predictions=[]
  for clf in classifiers:
    clf[0].fit(data_train, target_train)
    predictions.append( clf[0].predict(data_test))
    acc = accuracy_score(target_test, predictions[-1])
    avg_prec = precision_score(
        target_test, 
        predictions[-1], 
        average='weighted')
    avg_rec = recall_score(
        target_test, 
        predictions[-1], 
        average='weighted')
    avg_f1_score = f1_score(
        target_test, 
        predictions[-1], 
        average='weighted')
    report_entry=[
        tag, 
        len(data),
        len(predictions[-1]), 
        clf[1], 
        acc*100,
        avg_prec*100,
        avg_rec*100,
        avg_f1_score*100]
    if verbose:
      print report_entry
    final_report.loc[final_report.shape[0]] = report_entry
  print '\n\n\nfinal report:'
  print final_report
  return (predictions, target_test)
  
def write_final_report(output_path, label):
  if not os.path.exists(output_path):
      os.makedirs(output_path)
  sns.set_color_codes("muted")
  ''' printing report '''
  sns.barplot(
      x='Accuracy', 
      y='Classifier', 
      data=final_report, 
      color="b")
  plt.xlabel('Accuracy %')
  plt.title('Classifier Accuracy')
  plt.savefig('%s/final_report_wrist_%s_accuracy.png'%(
      output_path,
      label))
  final_report.to_excel(
      '%s/final_report_wrist_%s.xlsx'%(output_path,label))

if __name__== "__main__":
  main()
