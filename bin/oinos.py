#! /usr/bin/python
#http://kdd.ics.uci.edu/databases/eeg/eeg.html


from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, log_loss
from sklearn.svm import SVC, LinearSVC, NuSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.metrics import precision_recall_curve
from sklearn.utils.fixes import signature
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import csv
import glob
import random
import getopt
import sys
import os


classifiers = [
    (KNeighborsClassifier(3), 'K Nearest Neighbors' ),
    (SVC(kernel="linear", C=0.025, gamma='scale'), 'Linear SVM'),
    (SVC(gamma=2, C=1), 'RBF SVM'),
    (LinearSVC(), 'Linear SVC'),
#    (NuSVC(probability=True, gamma='scale'), 'Nu SVC'),
    (GaussianProcessClassifier(1.0 * RBF(1.0)), 'Gaussian Process'),
    (DecisionTreeClassifier(max_depth=5), 'Decision Tree depth 5'),
    (DecisionTreeClassifier(max_depth=10), 'Decision Tree depth 10'),
    (RandomForestClassifier(
        max_depth=5, 
        n_estimators=10, 
        max_features=1), 'Random Forest'),
    #(RandomForestClassifier(
    #    max_depth=10, 
    #    n_estimators=10, 
    #    max_features=30), 'Random Forest depth 10, features 30'),
    (GradientBoostingClassifier(), 'Gradient Boosting Classifier'),
    (MLPClassifier(alpha=1), 'Neural Net'),
    (AdaBoostClassifier(), 'Ada Boost'),
    (GaussianNB(), 'Naive Baies'),
    (LinearDiscriminantAnalysis(), 'Linear  Discriminant Analysis'),
    (QuadraticDiscriminantAnalysis(),'QDA')
    ]

final_report_cols=[
    'Dataset',
    'Dataset Size',
    'Test Size',
    'Classifier',
    'Accuracy',
    'AVG Precision',
    'AVG Recall',
    'F1 score']#,
    #'Log Loss']
final_report_ac= pd.DataFrame(columns=final_report_cols)
final_report_smn= pd.DataFrame(columns=final_report_cols)
final_report_acsmn= pd.DataFrame(columns=final_report_cols)
final_report_ac_ext= pd.DataFrame(columns=final_report_cols)


def main():
  try:
    opts, args = getopt.getopt(
        sys.argv[1:], 
        'hd:r:t:o:v', 
        ['help', '--datapath', '--ratio', '--target_mode'])
  except getopt.GetoptError as err:
    print str(err) 
    usage()
    sys.exit(2)
  output = None
  verbose = False
  ratio=0.2
  datapath=None
  tmode=1
  output_path='out'
  for o, a in opts:
    if o == '-v':
      verbose = True
    elif o in ('-h', '--help'):
      usage()
      sys.exit()
    elif o in ('-d', '--datapath'):
      datapath = a
    elif o in ('-r', '--ratio'):
      ratio = float(a)
    elif o in ('-o', '--output'):
      output_path = a
    else:
      assert False, 'unhandled option'
  if datapath is None:
    usage()
    sys.exit()
  learn(datapath, ratio, verbose)
  write_final_report(output_path, datapath)

def usage():
  usage_message='''usage:
  
  $ python bin/main.py -d <data_path> \
 -r <testing data ratio> \
 -o <output path>
  
example: 
  $ ./bin/oinos.py -d data_100 -r 0.3 -v -o out

  '''
  print usage_message
 

def learn(datapath, ratio, verbose):
  datapath='%s/*'%datapath
  eeg_files=glob.glob(datapath)
  random.shuffle(eeg_files)
  data = []
  target_ac = []
  target_smn = []
  target_acsmn = []
  for eeg_file in eeg_files:
    with open(eeg_file) as eeg:
      #data.append(eeg.read().splitlines())
      data.append(map(float, eeg))
    if eeg_file[len(datapath)+4:len(datapath)+5] == 'a':
      target_ac.append('ALC')
      if eeg_file[len(datapath)-1:len(datapath)+1] == '1s':
        target_acsmn.append('ALC-SNGL')
      elif eeg_file[len(datapath)-1:len(datapath)+1] == '2m':
        target_acsmn.append('ALC-MATCH')
      else:
        target_acsmn.append('ALC-NONMATCH')
    else:
      target_ac.append('CTRL')
      if eeg_file[len(datapath)-1:len(datapath)+1] == '1s':
        target_acsmn.append('CTRL-SNGL')
      elif eeg_file[len(datapath)-1:len(datapath)+1] == '2m':
        target_acsmn.append('CTRL-MATCH')
      else:
        target_acsmn.append('CTRL-NONMATCH')
    if eeg_file[len(datapath)-1:len(datapath)+1] == '1s':
      target_smn.append('SNGL')
    elif eeg_file[len(datapath)-1:len(datapath)+1] == '2m':
      target_smn.append('MATCH')
    else:
      target_smn.append('NONMATCH')
  dirty_records_indexes=[]
  for i in range (0,len(data)):
    if len(data[i]) <= 0:
      dirty_records_indexes.append(i)
  for i in range (0, len(dirty_records_indexes)):
    print 'deleting %d, target_ac %s'%(
        len(data[ dirty_records_indexes [-i-1]]),
        target_ac[ dirty_records_indexes [-i-1]])
    del data[ dirty_records_indexes [-i-1]]
    del target_ac[ dirty_records_indexes [-i-1]]
    del target_acsmn[ dirty_records_indexes [-i-1]]
    del target_smn[ dirty_records_indexes [-i-1]]
  if verbose:
    counter_a=0
    counter_c=0
    for ac in target_ac:
      if ac=='ALC':
        counter_a = counter_a+1
      else:
        counter_c = counter_c+1
    print 'alcoholics:%d'%counter_a
    print 'controls  :%d'%counter_c
  #data_smn_train, data_smn_test, target_smn_train, target_smn_test=\
  #    train_test_split(data, target_smn, test_size=ratio)
  #data_acsmn_train, data_acsmn_test, target_acsmn_train, target_acsmn_test=\
  #    train_test_split(data, target_acsmn, test_size=ratio)
  #print "  total data:%d\n\
  #train data:%d\n\
  #test data:%d"%(
  #    len(data),
  #    len(data_train),
  #    len(data_test))
  print '\n\n=========        ALCOHOLIC CONTROL         ============'
  learn_target(
      data, 
      final_report_ac,
      target_ac, 
      ratio, 
      'ALC-CTRL', 
      verbose)
  print '\n\n========= SINGLE - MATCHING - NON MATCHING ============'
  print '=========              IMAGES              ============'
  learn_target(
      data, 
      final_report_smn,
      target_smn, 
      ratio, 
      'SGL-MATCH-NONMATCH', 
      verbose)
  print '\n\n========= SINGLE - MATCHING - NON MATCHING ============'
  print '=========              IMAGES              ============'
  print '=========        ALCOHOLIC CONTROL         ============'
  predictions_acsmn, target_test_acsmn= learn_target(
      data, 
      final_report_acsmn,
      target_acsmn, 
      ratio, 
      'ALC-CTRL/SGL-MATCH-NONMATCH', 
      verbose)
  print '\n\n=========        ALCOHOLIC CONTROL         ============'
  print '=========             EXTENDED             ============'
  predictions_ext=infer_target(
      data, 
      predictions_acsmn, 
      target_test_acsmn,
      verbose)



def infer_target(data, predictions_acsmn, target_test, verbose):
  predictions_ext=[]
  tag='ALC-CTRL_EXT'
  target_test_ext=[]
  for pset in predictions_acsmn:
    p_ext=[]
    for p in pset:
      if p=='ALC-SNGL':
        p_ext.append('ALC')
      elif p=='ALC-MATCH':
        p_ext.append('ALC')
      elif p=='ALC-NONMATCH':
        p_ext.append('ALC')
      elif p=='CTRL-SNGL':
        p_ext.append('CTRL')
      elif p=='CTRL-MATCH':
        p_ext.append('CTRL')
      else:
        p_ext.append('CTRL')
    predictions_ext.append(p_ext)
  for t in target_test:
    if t=='ALC-SNGL':
      target_test_ext.append('ALC')
    elif t=='ALC-MATCH':
      target_test_ext.append('ALC')
    elif t=='ALC-NONMATCH':
      target_test_ext.append('ALC')
    elif t=='CTRL-SNGL':
      target_test_ext.append('CTRL')
    elif t=='CTRL-MATCH':
      target_test_ext.append('CTRL')
    else:
      target_test_ext.append('CTRL')
  for i in range (0, len(classifiers)):
    clf = classifiers[i]
    acc = accuracy_score(target_test_ext, predictions_ext[i])
    avg_prec = precision_score(
        target_test_ext, 
        predictions_ext[i], 
        average='weighted')
    avg_rec = recall_score(
        target_test_ext, 
        predictions_ext[i], 
        average='weighted')
    avg_f1_score = f1_score(
        target_test_ext, 
        predictions_ext[i], 
        average='weighted')
    report_entry=[
        tag, 
        len(data),
        len(predictions_ext[i]), 
        clf[1], 
        acc*100,
        avg_prec*100,
        avg_rec*100,
        avg_f1_score*100]
    if verbose:
      print report_entry
    final_report_ac_ext.loc[
        final_report_ac_ext.shape[0]] = report_entry
    #''' printing precision recall graph '''
    #print_precision_recall(
    #    target_test_ext,
    #    predictions_ext[i], 
    #    'ALC',
    #    tag,
    #    len(data),
    #    clf[1])
  print '\n\n\nfinal report:'
  print final_report_ac_ext

def learn_target(data,final_report, target , ratio, tag, verbose):  
  data_train, data_test, target_train, target_test=\
      train_test_split(data, target, test_size=ratio)
  predictions=[]
  for clf in classifiers:
    clf[0].fit(data_train, target_train)
    predictions.append( clf[0].predict(data_test))
    acc = accuracy_score(target_test, predictions[-1])
    avg_prec = precision_score(
        target_test, 
        predictions[-1], 
        average='weighted')
    avg_rec = recall_score(
        target_test, 
        predictions[-1], 
        average='weighted')
    avg_f1_score = f1_score(
        target_test, 
        predictions[-1], 
        average='weighted')
    #train_predictions = clf[0].predict_proba(data_test)
    #ll = log_loss(target_test, train_predictions)
    #report_entry=[tag, len(data), clf[1], acc*100, ll]
    report_entry=[
        tag, 
        len(data),
        len(predictions[-1]), 
        clf[1], 
        acc*100,
        avg_prec*100,
        avg_rec*100,
        avg_f1_score*100]
    if verbose:
      print report_entry
    final_report.loc[final_report.shape[0]] = report_entry
  print '\n\n\nfinal report:'
  print final_report
  return (predictions, target_test)
  
def write_final_report(output_path, label):
  print 'output_path:%s'%output_path
  if not os.path.exists(output_path):
      os.makedirs(output_path)
  sns.set_color_codes("muted")
  ''' printing ac report '''
  sns.barplot(
      x='Accuracy', 
      y='Classifier', 
      data=final_report_ac, 
      color="b")
  plt.xlabel('Accuracy %')
  plt.title('Classifier Accuracy')
  plt.savefig('%s/final_report_ac_%s_accuracy.png'%(
      output_path,
      label))
  ''' printing smn report '''
  sns.barplot(
      x='Accuracy', 
      y='Classifier', 
      data=final_report_smn, 
      color="b")
  plt.xlabel('Accuracy %')
  plt.title('Classifier Accuracy')
  plt.savefig('%s/final_report_smn_%s_accuracy.png'%(
      output_path,
      label))
  ''' printing acsmn report '''
  sns.barplot(
      x='Accuracy', 
      y='Classifier', 
      data=final_report_acsmn, 
      color="b")
  plt.xlabel('Accuracy %')
  plt.title('Classifier Accuracy')
  plt.savefig('%s/final_report_acsmn_%s_accuracy.png'%(
      output_path,
      label))
  ''' printing ac_ext report '''
  sns.barplot(
      x='Accuracy', 
      y='Classifier', 
      data=final_report_ac_ext, 
      color="b")
  plt.xlabel('Accuracy %')
  plt.title('Classifier Accuracy')
  plt.savefig('%s/final_report_ac_ext_%s_accuracy.png'%(
      output_path,
      label))
  #sns.set_color_codes("muted")
  #sns.barplot(x='Log Loss', y='Classifier', data=final_report, color="g")
  #plt.xlabel('Log Loss')
  #plt.title('Classifier Log Loss')  
  #plt.savefig('final_report_%s_logloss.png'%label)
  final_report_ac.to_excel(
      '%s/final_report_ac_%s.xlsx'%(output_path,label))
  final_report_smn.to_excel(
      '%s/final_report_smn_%s.xlsx'%(output_path,label))
  final_report_acsmn.to_excel(
      '%s/final_report_acsmn_%s.xlsx'%(output_path,label))
  final_report_ac_ext.to_excel(
      '%s/final_report_ac_ext_%s.xlsx'%(output_path,label))


if __name__== "__main__":
  main()
